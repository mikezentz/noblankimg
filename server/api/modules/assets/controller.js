import fs from 'fs'
import path from 'path'
import stream from 'stream'
import AssetsProvider from '../../providers/assets'
import AssetMetaProvider from '../../providers/assetMeta'
import IpfsProvider from '../../providers/ipfs'
import Web3Provider from '../../providers/web3'
import AmberdataProvider from '../../providers/amberdata'

const Assets = new AssetsProvider()
const AssetMeta = new AssetMetaProvider()
const Amberdata = new AmberdataProvider()
const ipfs = new IpfsProvider()
const web3 = new Web3Provider()

const handleAssetType = hash => {
  const hasDot = hash.includes('.')
  return hasDot ? hash.split('.') : [hash]
}

const getAssetUrl = hash => {
  return `https://ipfs.infura.io:5001/api/v0/object/data?arg=${hash}`
}

const getAssetPath = hash => {
  return `https://api.noblankimg.com/v1/assets/${hash}.png`
}

class Controller {
  async create(req, res) {
    // TODO: Validate data!!!!
    const data = await ipfs.upload(new Buffer.from(req.files[0].buffer))
    res.json(data)
  }

  async byId(req, res) {
    const C = new Controller()
    const hash = req.params.id
    const options = {
      w: req.query && req.query.w ? req.query.w : 128,
      h: req.query && req.query.h ? req.query.h : 128,
    }
    // options.path = path.join(__dirname, '../../../../static/test_image.jpg')

    try {
      // If is address, return fallbackImage directly
      // Otherwise, check ipfs, if no image return fallbackImage
      const addressData = await Amberdata.getAddress(hash)
      console.log('addressData', addressData)
      if (addressData.addressType === 'address') {
        return C.byIdImageFallback(req, res)
      }

      // TODO: - get ipfs hash from web3, if none found return fallback
      // TODO: - get img from local cache
      // - if no cache, get from IPFS and cache, return asset
      const ref = handleAssetType(hash)
      console.log('ref', ref)
      const files = await ipfs.byHash(ref[0])

      if (!files) {
        return C.byIdImageFallback(req, res)
      }
      options.content = files[0].content
      options.type = ref[1] || 'png'

      Assets.image(options).pipe(res)
    } catch (e) {
      res.send('')
    }
  }

  async metaById(req, res) {
    const hash = req.params.id
    const options = {}
    try {
      const addressData = await Amberdata.getAddress(hash)
      addressData.assetPath = getAssetPath(hash)
      console.log('addressData', addressData)

      const hashRef = await web3.getHashReference(hash)
      addressData.verified = hashRef.verified
      console.log('hashRef', hashRef)
      if (!hashRef) {
        res.json(addressData)
        return
      }

      options.path = path.join(__dirname, `../../../../static/${hash}.png`)
      // options.path = getAssetUrl(hashRef)
      const palette = await AssetMeta.image(options)
      addressData.brand = palette
      console.log('palette', palette)

      res.json(addressData)
    } catch (e) {
      console.log('e', e)
      res.send('[]')
    }
  }

  async byIdImageFallback(req, res) {
    try {
      console.log('req.params.id', req.params.id)
      const ref = handleAssetType(req.params.id)
      console.log('ref', ref[0])
      const fallback = await Assets.fallbackImage(ref[0])
      const bufferStream = new stream.PassThrough()
      bufferStream.end(new Buffer.from(fallback))

      return bufferStream.pipe(res)
    } catch (e) {
      console.log('e', e)
      res.send('')
    }
  }

  updateId(req) {
    // TODO: Validate required data
    return ''
  }

  removeId(req) {
    // TODO: Validate required data
    return ''
  }
}

export default new Controller()
