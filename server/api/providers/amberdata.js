// import fs from 'fs'
import axios from 'axios'

const blockchainId = '1c9c969065fcd1cf'
const baseUrl = `https://${blockchainId}.api.amberdata.io`
const baseOptions = {
  headers: {
    'x-amberdata-blockchain-id': blockchainId
  }
}

class AmberdataProvider {
  constructor() {
    return this
  }

  getAddress(hash) {
    return axios.get(`${baseUrl}/address/${hash}`, baseOptions)
      .then(res => {
        const data = res.data
        if (!data) return {}
        return {
          addressType: data.addressType,
          contractType: data.contractType,
          createdAt: data.createdAt,
          creationBlock: data.creationBlock,
        }
      })
      .catch(() => {
        return {}
      })
  }
}

export default AmberdataProvider
