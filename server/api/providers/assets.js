import fs from 'fs'
import stream from 'stream'
import sharp from 'sharp'
import jdenticon from 'jdenticon'

class AssetsProvider {
  constructor() {
    return this
  }

  fallbackImage(hash) {
    return jdenticon.toPng(hash, 256)
  }

  image(options) {
    const bufferStream = new stream.PassThrough()
    bufferStream.end(new Buffer.from(options.content))
    let s = sharp()

    s = s.resize(parseInt(options.w, 10), parseInt(options.h, 10))

    if (options.type === 'png') s = s.png()
    if (options.type === 'jpg' || options.type === 'jpeg') {
      s = s.jpeg({
        quality: 100,
        chromaSubsampling: '4:4:4',
      })
    }

    // TODO: Setup for invalidated images
    // .blur(30)
    // .toColourspace('b-w')

    return bufferStream.pipe(s)
  }
}

export default AssetsProvider
