require('source-map-support/register')
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("stream");

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(5);


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

__webpack_require__(6);

var _server = __webpack_require__(8);

var _server2 = _interopRequireDefault(_server);

var _routes = __webpack_require__(16);

var _routes2 = _interopRequireDefault(_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = new _server2.default().router(_routes2.default).listen(parseInt(process.env.PORT, 10));

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _dotenv = __webpack_require__(7);

var _dotenv2 = _interopRequireDefault(_dotenv);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_dotenv2.default.config();

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("dotenv");

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = __webpack_require__(2);

var _express2 = _interopRequireDefault(_express);

var _path = __webpack_require__(0);

var path = _interopRequireWildcard(_path);

var _bodyParser = __webpack_require__(9);

var bodyParser = _interopRequireWildcard(_bodyParser);

var _http = __webpack_require__(10);

var http = _interopRequireWildcard(_http);

var _os = __webpack_require__(11);

var os = _interopRequireWildcard(_os);

var _cors = __webpack_require__(12);

var _cors2 = _interopRequireDefault(_cors);

var _cookieParser = __webpack_require__(13);

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _logger = __webpack_require__(14);

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = new _express2.default();

class Server {
  constructor() {
    const root = path.normalize(`${__dirname}/../..`);
    app.use(bodyParser.json({ limit: '5mb' }));
    app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));
    app.use((0, _cookieParser2.default)(process.env.SESSION_SECRET));
    app.use(_express2.default.static(`${root}/public`));
    app.use((0, _cors2.default)());
    app.disable('x-powered-by');
    app.enable('case sensitive routing');
    app.enable('strict routing');
  }

  router(routes) {
    routes(app);
    return this;
  }

  listen(port = parseInt(process.env.PORT, 10)) {
    const welcome = p => () => _logger2.default.info(`up and running in ${"development" || 'development'} @: ${os.hostname()} on port: ${p}}`);
    http.createServer(app).listen(port, welcome(port));
    return app;
  }
}
exports.default = Server;
/* WEBPACK VAR INJECTION */}.call(exports, "server/common"))

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("http");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("os");

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("cors");

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("cookie-parser");

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pino = __webpack_require__(15);

var _pino2 = _interopRequireDefault(_pino);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const l = (0, _pino2.default)({
  name: process.env.APP_ID || 'APP',
  level: process.env.LOG_LEVEL || 1
});

exports.default = l;

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("pino");

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = routes;

var _router = __webpack_require__(17);

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const baseUri = '/v1';

function routes(app) {
  app.use(`${baseUri}/assets`, _router2.default);
}

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = __webpack_require__(2);

var express = _interopRequireWildcard(_express);

var _multer = __webpack_require__(18);

var _multer2 = _interopRequireDefault(_multer);

var _controller = __webpack_require__(19);

var _controller2 = _interopRequireDefault(_controller);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

// handle multi-part
const upload = (0, _multer2.default)();

exports.default = express.Router().post('/', upload.any(), _controller2.default.create).get('/:id/fallback', _controller2.default.byIdImageFallback).get('/:id/meta', _controller2.default.metaById).get('/:id', _controller2.default.byId).patch('/:id', _controller2.default.updateId).delete('/:id', _controller2.default.removeId);

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = require("multer");

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = __webpack_require__(1);

var _fs2 = _interopRequireDefault(_fs);

var _path = __webpack_require__(0);

var _path2 = _interopRequireDefault(_path);

var _stream = __webpack_require__(3);

var _stream2 = _interopRequireDefault(_stream);

var _assets = __webpack_require__(20);

var _assets2 = _interopRequireDefault(_assets);

var _assetMeta = __webpack_require__(23);

var _assetMeta2 = _interopRequireDefault(_assetMeta);

var _ipfs = __webpack_require__(25);

var _ipfs2 = _interopRequireDefault(_ipfs);

var _web = __webpack_require__(29);

var _web2 = _interopRequireDefault(_web);

var _amberdata = __webpack_require__(31);

var _amberdata2 = _interopRequireDefault(_amberdata);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const Assets = new _assets2.default();
const AssetMeta = new _assetMeta2.default();
const Amberdata = new _amberdata2.default();
const ipfs = new _ipfs2.default();
const web3 = new _web2.default();

const handleAssetType = hash => {
  const hasDot = hash.includes('.');
  return hasDot ? hash.split('.') : [hash];
};

const getAssetUrl = hash => {
  return `https://ipfs.infura.io:5001/api/v0/object/data?arg=${hash}`;
};

const getAssetPath = hash => {
  return `https://api.noblankimg.com/v1/assets/${hash}.png`;
};

class Controller {
  async create(req, res) {
    // TODO: Validate data!!!!
    const data = await ipfs.upload(new Buffer.from(req.files[0].buffer));
    res.json(data);
  }

  async byId(req, res) {
    const C = new Controller();
    const hash = req.params.id;
    const options = {
      w: req.query && req.query.w ? req.query.w : 128,
      h: req.query && req.query.h ? req.query.h : 128
      // options.path = path.join(__dirname, '../../../../static/test_image.jpg')

    };try {
      // If is address, return fallbackImage directly
      // Otherwise, check ipfs, if no image return fallbackImage
      const addressData = await Amberdata.getAddress(hash);
      console.log('addressData', addressData);
      if (addressData.addressType === 'address') {
        return C.byIdImageFallback(req, res);
      }

      // TODO: - get ipfs hash from web3, if none found return fallback
      // TODO: - get img from local cache
      // - if no cache, get from IPFS and cache, return asset
      const ref = handleAssetType(hash);
      console.log('ref', ref);
      const files = await ipfs.byHash(ref[0]);

      if (!files) {
        return C.byIdImageFallback(req, res);
      }
      options.content = files[0].content;
      options.type = ref[1] || 'png';

      Assets.image(options).pipe(res);
    } catch (e) {
      res.send('');
    }
  }

  async metaById(req, res) {
    const hash = req.params.id;
    const options = {};
    try {
      const addressData = await Amberdata.getAddress(hash);
      addressData.assetPath = getAssetPath(hash);
      console.log('addressData', addressData);

      const hashRef = await web3.getHashReference(hash);
      addressData.verified = hashRef.verified;
      console.log('hashRef', hashRef);
      if (!hashRef) {
        res.json(addressData);
        return;
      }

      options.path = _path2.default.join(__dirname, `../../../../static/${hash}.png`);
      // options.path = getAssetUrl(hashRef)
      const palette = await AssetMeta.image(options);
      addressData.brand = palette;
      console.log('palette', palette);

      res.json(addressData);
    } catch (e) {
      console.log('e', e);
      res.send('[]');
    }
  }

  async byIdImageFallback(req, res) {
    try {
      console.log('req.params.id', req.params.id);
      const ref = handleAssetType(req.params.id);
      console.log('ref', ref[0]);
      const fallback = await Assets.fallbackImage(ref[0]);
      const bufferStream = new _stream2.default.PassThrough();
      bufferStream.end(new Buffer.from(fallback));

      return bufferStream.pipe(res);
    } catch (e) {
      console.log('e', e);
      res.send('');
    }
  }

  updateId(req) {
    // TODO: Validate required data
    return '';
  }

  removeId(req) {
    // TODO: Validate required data
    return '';
  }
}

exports.default = new Controller();
/* WEBPACK VAR INJECTION */}.call(exports, "server/api/modules/assets"))

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = __webpack_require__(1);

var _fs2 = _interopRequireDefault(_fs);

var _stream = __webpack_require__(3);

var _stream2 = _interopRequireDefault(_stream);

var _sharp = __webpack_require__(21);

var _sharp2 = _interopRequireDefault(_sharp);

var _jdenticon = __webpack_require__(22);

var _jdenticon2 = _interopRequireDefault(_jdenticon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class AssetsProvider {
  constructor() {
    return this;
  }

  fallbackImage(hash) {
    return _jdenticon2.default.toPng(hash, 256);
  }

  image(options) {
    const bufferStream = new _stream2.default.PassThrough();
    bufferStream.end(new Buffer.from(options.content));
    let s = (0, _sharp2.default)();

    s = s.resize(parseInt(options.w, 10), parseInt(options.h, 10));

    if (options.type === 'png') s = s.png();
    if (options.type === 'jpg' || options.type === 'jpeg') {
      s = s.jpeg({
        quality: 100,
        chromaSubsampling: '4:4:4'
      });
    }

    // TODO: Setup for invalidated images
    // .blur(30)
    // .toColourspace('b-w')

    return bufferStream.pipe(s);
  }
}

exports.default = AssetsProvider;

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = require("sharp");

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = require("jdenticon");

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
const getColors = __webpack_require__(24);

class AssetMetaProvider {
  constructor() {
    return this;
  }

  async image(options) {
    const colors = await getColors(options.path);
    return colors.map(color => color.hex());
  }
}

exports.default = AssetMetaProvider;

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = require("get-image-colors");

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ipfsApi = __webpack_require__(26);

var _ipfsApi2 = _interopRequireDefault(_ipfsApi);

var _multihash = __webpack_require__(27);

var _multihash2 = _interopRequireDefault(_multihash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Looks like IPFS on infura doesn't require API key yet
const apiKey = process.env.INFURA_API_KEY || '';

// TODO: Might not needed
// const fileOptions = {
//   pin: true,
//   wrapWithDirectory: 'test'
// }

class IpfsProvider {
  constructor() {
    this.ipfs = (0, _ipfsApi2.default)('ipfs.infura.io', '5001', { protocol: 'https' });
    return this;
  }

  async upload(bufferData) {
    return new Promise((resolve, reject) => {
      this.ipfs.files.add(bufferData, async (err, files) => {
        if (err) return reject(err);
        const hash = files[0].hash;
        const fileData = await _multihash2.default.getBytes32FromMultiash(hash);
        fileData.hash = hash;

        resolve(fileData);
      });
    });
  }

  async byHash(hash) {
    return new Promise((resolve, reject) => {
      try {
        this.ipfs.files.get(hash, (err, files) => {
          if (err) return resolve(null);
          return resolve(files);
        });
      } catch (e) {
        reject(e);
      }
    });
  }
}

exports.default = IpfsProvider;

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = require("ipfs-api");

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


const bs58 = __webpack_require__(28);

const self = module.exports = {
  /**
   * @typedef {Object} Multihash
   * @property {string} digest The digest output of hash function in hex with prepended '0x'
   * @property {number} hashFunction The hash function code for the function used
   * @property {number} size The length of digest
   */

  /**
   * Partition multihash string into object representing multihash
   *
   * @param {string} multihash A base58 encoded multihash string
   * @returns {Multihash}
   */
  getBytes32FromMultiash(multihash) {
    const decoded = bs58.decode(multihash);
    const data = decoded.slice(2);
    const digest = `0x${Buffer.from(data, 'utf8').toString('hex')}`;

    return {
      digest,
      hashFunction: decoded[0],
      size: decoded[1]
    };
  },

  /**
   * Encode a multihash structure into base58 encoded multihash string
   *
   * @param {Multihash} multihash
   * @returns {(string|null)} base58 encoded multihash string
   */
  getMultihashFromBytes32(multihash) {
    const { digest, hashFunction, size } = multihash;
    if (size === 0) return null;

    // cut off leading "0x"
    const hashBytes = Buffer.from(digest.slice(2), 'hex');

    // prepend hashFunction and digest size
    const multihashBytes = new hashBytes.constructor(2 + hashBytes.length);
    multihashBytes[0] = hashFunction;
    multihashBytes[1] = size;
    multihashBytes.set(hashBytes, 2);

    return bs58.encode(multihashBytes);
  },

  /**
   * Parse Solidity response in array to a Multihash object
   *
   * @param {object} response Response array from Solidity
   * @returns {Multihash} multihash object
   */
  parseContractResponse(response) {
    const { digest, hashFunction, size } = response;
    return {
      digest,
      hashFunction,
      size
    };
  },

  /**
   * Parse Solidity response in array to a base58 encoded multihash string
   *
   * @param {array} response Response array from Solidity
   * @returns {string} base58 encoded multihash string
   */
  getMultihashFromContractResponse(response) {
    return self.getMultihashFromBytes32(self.parseContractResponse(response));
  }
};

/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = require("bs58");

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = __webpack_require__(1);

var _fs2 = _interopRequireDefault(_fs);

var _path = __webpack_require__(0);

var _path2 = _interopRequireDefault(_path);

var _web = __webpack_require__(30);

var _web2 = _interopRequireDefault(_web);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const godAddress = '0x0000000000000000000000000000000000000000000000000000000000000000';
const ContractAbiPath = _path2.default.join(__dirname, '../../../', 'build/contracts/NoBlankImg.json');

// Get an API key from https://infura.io/
const apiKey = process.env.INFURA_API_KEY || '';
const network = process.env.ETH_NETWORK || '';
const rpcUrl = () => {
  if (true) return `http://localhost:8545`;
  return `https://${network}.infura.io/v3/${apiKey}`;
};

const getAbiDeployedAddress = abi => {
  const networks = abi.networks;
  return networks[Object.keys(networks)[0]].address;
};

class Web3Provider {
  constructor() {
    this.web3 = new _web2.default(new _web2.default.providers.HttpProvider(rpcUrl()));

    try {
      this.AbiFile = JSON.parse(_fs2.default.readFileSync(ContractAbiPath, 'utf8'));
      if (this.AbiFile) this.deployedAddress = getAbiDeployedAddress(this.AbiFile);
      this.Contract = new this.web3.eth.Contract(this.AbiFile.abi, this.deployedAddress);
    } catch (e) {
      console.log('e', e);
      console.error('Could not load contract ABI file, please check address or redeploy!');
    }

    return this;
  }

  getHashReference(hash) {
    return new Promise((resolve, reject) => {
      // this.Contract.methods.verifiedAsset(hash).call((e, r) => {
      //   if (e) return reject(e)
      //   if (r === godAddress) return resolve(null)
      //   resolve(r)
      // })
      this.Contract.methods.getAssetByHash(hash).call((e, r) => {
        if (e) return reject(e);
        console.log('r', r['0'], r['1'], _web2.default.utils.hexToAscii(r['0']));
        const hashRef = r['0'];
        const verified = r['1'] !== godAddress && r['1'] === hashRef;
        resolve({
          hash: hashRef,
          verified
        });
      });
    });
  }
}

exports.default = Web3Provider;
/* WEBPACK VAR INJECTION */}.call(exports, "server/api/providers"))

/***/ }),
/* 30 */
/***/ (function(module, exports) {

module.exports = require("web3");

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _axios = __webpack_require__(32);

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const blockchainId = '1c9c969065fcd1cf'; // import fs from 'fs'

const baseUrl = `https://${blockchainId}.api.amberdata.io`;
const baseOptions = {
  headers: {
    'x-amberdata-blockchain-id': blockchainId
  }
};

class AmberdataProvider {
  constructor() {
    return this;
  }

  getAddress(hash) {
    return _axios2.default.get(`${baseUrl}/address/${hash}`, baseOptions).then(res => {
      const data = res.data;
      if (!data) return {};
      return {
        addressType: data.addressType,
        contractType: data.contractType,
        createdAt: data.createdAt,
        creationBlock: data.creationBlock
      };
    }).catch(() => {
      return {};
    });
  }
}

exports.default = AmberdataProvider;

/***/ }),
/* 32 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ })
/******/ ]);
//# sourceMappingURL=main.map